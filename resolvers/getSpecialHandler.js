import SpecialResponse from "../response/SpecialResponse";

export default function (args, ctx) {
     const response = {
        is_load_menus: true,
        is_load_settings: true,
        is_load_data_type: true
    }
    return SpecialResponse( response )
}