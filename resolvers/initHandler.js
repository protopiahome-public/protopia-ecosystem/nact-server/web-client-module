import getClient from "../../main-module/oauth/helpers/getClient";
import InitResponse from "../response/InitResponse";

export default function (args, ctx) {
    const client = getClient(ctx);

    const response = {
        menu: { 
            json: [ {} ]
        },
        widgets: { 
            json: {} 
        },
        template: { 
            json: {}
        },
        folder: { 
            json: {} 
        },
        public_options: { 
            json: {
                parseks: parseInt(Math.random() * 60),
                planets: parseInt(Math.random() * 100)
            }
        },
        version: "1"
    }
    return InitResponse( response )
}