import changeTemplateHandler from './changeTemplateHandler';
import getOptionsHandler from './getOptionsHandler';
import getSpecialHandler from './getSpecialHandler';
import getTemplateHandler from './getTemplateHandler';
import initHandler from './initHandler'

module.exports = {

  Mutation: { 
    changeTemplate: async (obj, args, ctx, info) => changeTemplateHandler(args.input, ctx) 
  },
  Query: { 
    /*
    *   web client layout and routing data.
    *   This query requested only in Extended method (see layouts.json on web client).
    *   Edit this data in web admin panel.
    */
    getInit: async (obj, args, ctx, info) => initHandler(args, ctx),
    getSpecial: async (obj, args, ctx, info) => getSpecialHandler(args, ctx),
    getTemplate: async (obj, args, ctx, info) => getTemplateHandler(args, ctx),
    getOptions: async (obj, args, ctx, info) => getOptionsHandler(args, ctx),
  },
};

