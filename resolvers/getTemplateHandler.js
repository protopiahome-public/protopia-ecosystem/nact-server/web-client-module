import getTemplateResponse from "../response/getTemplateResponse";

export default function (args, ctx) {
     const response = {
        json: JSON.stringify(
            {
                icon: "fas fa-home", 
                icon_width: 100, 
                icon_height: 100, 
                header: true, 
                left_menu: "pictogramm"
            }
        ) 
    }
    return getTemplateResponse( response )
}