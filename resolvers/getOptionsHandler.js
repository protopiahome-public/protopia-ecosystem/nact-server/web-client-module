
import OptionsResponse from "../response/OptionsResponse";

export default function (args, ctx) {
     const response = {
        id: args.id,
        name: "Client", 
        description: "",
        email: "",
        address: "",
        help_url: "",
        vk_app_id: "club219282968",
        vk: "https://vk.com/krfest_retro",
        youtube: "https://www.youtube.com/watch?v=5AyZFNYr45g",
        default_img_name: "85d04e1a62b8ec22445bf76ee2270044.jpg",
        default_img_id: "",
        default_img: "https://i.pinimg.com/564x/85/d0/4e/85d04e1a62b8ec22445bf76ee2270044.jpg",
        thumbnail_name: "325609.png",
        thumbnail_id: "",
        thumbnail: "https://cdn-icons-png.flaticon.com/128/325/325609.png",
        user_verify_account: true 
    }
    return OptionsResponse( response )
}

